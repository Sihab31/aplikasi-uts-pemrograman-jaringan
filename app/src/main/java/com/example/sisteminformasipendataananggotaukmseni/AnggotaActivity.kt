package com.example.sisteminformasipendataananggotaukmseni

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.AdapterView
import android.widget.SimpleAdapter
import android.widget.Toast
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.android.synthetic.main.activity_anggota.*

class AnggotaActivity : AppCompatActivity(), View.OnClickListener {

    val COLLECTION = "member"
    val F_NIM = "nim"
    val F_MHS = "mhs"
    val F_JURUSAN = "jurusan"
    val F_DIVISI = "divisi"
    var docId = ""
    lateinit var db : FirebaseFirestore
    lateinit var alMember : ArrayList<HashMap<String,Any>>
    lateinit var adapter : SimpleAdapter

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btnBackAnggota -> {
                val intent = Intent(this,SignInActivity::class.java)
                startActivity(intent)
            }
            R.id.btnTambah ->{
                val hm = HashMap<String,Any>()
                hm.set(F_NIM,edNIM.text.toString())
                hm.set(F_MHS,edMhs.text.toString())
                hm.set(F_JURUSAN,edJurusan.text.toString())
                hm.set(F_DIVISI,edDivisi.text.toString())
                db.collection(COLLECTION).document(edNIM.text.toString()).set(hm).addOnSuccessListener {
                    Toast.makeText(this, "Tambah Data Berhasil", Toast.LENGTH_SHORT).show()
                }.addOnFailureListener { e ->
                    Toast.makeText(this, "Tambah Data Gagal  ${e.message}", Toast.LENGTH_SHORT).show()
                }
            }
            R.id.btnUbah ->{
                val hm = HashMap<String,Any>()
                hm.set(F_NIM,docId)
                hm.set(F_MHS,edMhs.text.toString())
                hm.set(F_JURUSAN,edJurusan.text.toString())
                hm.set(F_DIVISI,edDivisi.text.toString())
                db.collection(COLLECTION).document(docId).update(hm)
                    .addOnSuccessListener {
                        Toast.makeText(this, "Update Data Berhasil", Toast.LENGTH_SHORT).show()
                    }
                    .addOnFailureListener { e ->
                        Toast.makeText(this, "Update Data Gagal  ${e.message}", Toast.LENGTH_SHORT).show()
                    }
            }
            R.id.btnHapus ->{
                db.collection(COLLECTION).whereEqualTo(F_NIM,docId).get().addOnSuccessListener {
                        result ->
                    for (doc in result){
                        db.collection(COLLECTION).document(docId).delete().addOnSuccessListener {
                            Toast.makeText(this, "Delete Data Berhasil", Toast.LENGTH_SHORT).show()
                        }.addOnFailureListener { e ->
                            Toast.makeText(this, "Delete Data Gagal  ${e.message}", Toast.LENGTH_SHORT).show()
                        }
                    }
                }.addOnFailureListener { e ->
                    Toast.makeText(this, "Can't get data's references ${e.message}", Toast.LENGTH_SHORT).show()
                }
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_anggota)
        btnBackAnggota.setOnClickListener(this)

        alMember = ArrayList()
        btnTambah.setOnClickListener(this)
        btnUbah.setOnClickListener(this)
        btnHapus.setOnClickListener(this)
        lsData.setOnItemClickListener(itemClik)
    }

    override fun onStart() {
        super.onStart()
        db = FirebaseFirestore.getInstance()
        db.collection(COLLECTION).addSnapshotListener { querySnapshot, e ->
            if (e != null) Log.d("fireStore", e.message)
            showData()
        }
    }

    val itemClik = AdapterView.OnItemClickListener { parent, view, position, id ->
        val hm = alMember.get(position)
        docId = hm.get(F_NIM).toString()
        edNIM.setText(docId)
        edMhs.setText(hm.get(F_MHS).toString())
        edJurusan.setText(hm.get(F_JURUSAN).toString())
        edDivisi.setText(hm.get(F_DIVISI).toString())
    }

    fun showData(){
        db.collection(COLLECTION).get().addOnSuccessListener { result ->
            alMember.clear()
            for (doc in result) {
                val hm = HashMap<String, Any>()
                hm.set(F_NIM, doc.get(F_NIM).toString())
                hm.set(F_MHS, doc.get(F_MHS).toString())
                hm.set(F_JURUSAN, doc.get(F_JURUSAN).toString())
                hm.set(F_DIVISI, doc.get(F_DIVISI).toString())
                alMember.add(hm)
            }
            adapter = SimpleAdapter(
                this, alMember, R.layout.row_data,
                arrayOf(F_NIM, F_MHS, F_JURUSAN, F_DIVISI),
                intArrayOf(R.id.txNIM, R.id.txMhs, R.id.txJurusan, R.id.txDivisi)
            )
            lsData.adapter = adapter
        }
    }
}
